/**************************
*Damyon Olson
*CPSC 1021, Section 004, F20
*damyono@g.clemson.edu
*Section TA: Elliot McMillan, Matt Franchi
**************************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

/*The following is a struct provided by the instructors.*/
typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

/*Function prototypes, and myrandom*/
bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {

  /*This is to seed the random generator*/
  srand(unsigned (time(0)));


  /*This section of code will create an array and then procede to initialize
	each part of the struct 10 times.*/
	employee empArr[10];

	for (int i = 0; i < 10; i++){
		cout << "Please Enter Employee " << i+1 <<"'s First Name: ";
		cin >> empArr[i].firstName;
		cout << "Please Enter Employee " << i + 1 << "'s Last Name: ";
		cin >> empArr[i].lastName;
		cout << "Please Enter Employee " << i + 1 << "'s Year Of Birth: ";
		cin >> empArr[i].birthYear;
		cout << "Please Enter Employee " << i + 1 << "'s Hourly Wage: ";
		cin >> empArr[i].hourlyWage;
		cout << endl;
	}//End for

  /*The following three lines of code randomly shuffle the newly initialized
	employees.*/
	 employee *pointFirst = &empArr[0];
	 employee *pointLast  = (empArr + 10);
	 random_shuffle(pointFirst, pointLast, myrandom);

   /*The following line of code creates a new array with the five randomly
	 shuffled employees that were originally initialized.*/
		employee empList[5] = {empArr[0], empArr[1], empArr[2], empArr[3], empArr[4]};

    /*The following few lines of code sorts the list of five new arrays.*/
		 pointFirst = empList;
		 pointLast = (empList + 5);
		 sort(pointFirst, pointLast, name_order);

    /*The following for loop prints the array.*/
		for (int i = 0; i < 5; i++){
			cout << empList[i].lastName << ", " << empList[i].firstName << endl;
			cout << empList[i].birthYear << endl;
			cout << showpoint << setprecision(3) << empList[1].hourlyWage << endl;
			cout << "\n";
		}//End for

  return 0;
}//End main


/*The following determines the order of which the last names will be
sorted. They will be in order from A to Z*/
bool name_order(const employee& lhs, const employee& rhs) {
	if ((lhs.lastName).compare(rhs.lastName) < 0) {
		return true;
	}//End if
	else if ((lhs.lastName).compare(rhs.lastName) > 0){
		return false;
	}//Ens else if
	else {
		exit(1);
	}//End else
}//End name_order
